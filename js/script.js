var swiper = new Swiper(".projects-content", {
    slidesPerView = 3,
    spaceBetween = 30,
    slidesPerGroup = 3,
    loop = true,
    loopFillGroupWithBlank = true,
    pagination: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});
